module.exports = {
  projectName: "vue-demo",
  privateKey: "/Users/houhoz/.ssh/id_rsa",
  passphrase: "",
  cluster: [],
  dev: {
    name: "开发环境",
    script: "npm run build",
    host: "123.57.100.20",
    port: 22,
    username: "root",
    password: "HoHo123456",
    distPath: "dist",
    webDir: "/usr/web/www/vue-demo",
    isRemoveRemoteFile: true
  },
  prod: {
    name: "生产环境",
    script: "npm run build",
    host: "123.57.100.20",
    port: 22,
    username: "root",
    password: "HoHo123456",
    distPath: "dist",
    webDir: "/usr/web/www/vue-demo",
    isRemoveRemoteFile: true
  }
};
