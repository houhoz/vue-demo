# 这个有node依赖缓存
FROM node:12-alpine
WORKDIR /app
COPY package* yarn.lock ./
RUN npm install -g cnpm --registry=https://registry.npmmirror.com
RUN cnpm install
COPY public ./public
COPY src ./src
COPY .eslintrc.js ./
RUN npm run build

FROM nginx:alpine
RUN mkdir /app
COPY --from=0 /app/dist /app
COPY nginx.conf /etc/nginx/nginx.conf

# 这个没有， 不知道为什么
# FROM node:12-alpine
# COPY ./ /app
# WORKDIR /app
# RUN npm install
# RUN npm run build

# FROM nginx:alpine
# RUN mkdir /app
# COPY --from=0 /app/dist /app
# COPY nginx.conf /etc/nginx/nginx.conf

