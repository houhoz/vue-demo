export const debounce = (func, wait) => {
  let timeout;
  let canceled = false;
  const f = function(...args) {
    if (canceled) return;
    clearTimeout(timeout);
    timeout = setTimeout(() => func.call(this, ...args), wait);
  };
  f.cancel = () => {
    clearTimeout(timeout);
    canceled = true;
  };
  return f;
};

