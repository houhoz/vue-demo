# vue-demo

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### 发布
```
#init
npx deploy-cli-service i           

#开发环境
npx deploy-cli-service d --mode dev

#线上环境
npx deploy-cli-service d --mode prod
```
[自动化部署插件](https://github.com/fuchengwei/deploy-cli-service)

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
